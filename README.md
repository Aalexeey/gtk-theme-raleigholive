An Olive version of the old Raleigh theme for GTK+ 2 &amp; GTK+ 3
![2022-07-14_23-14-04](https://codeberg.org/Aalexeey/gtk-theme-raleigholive/raw/branch/main/2022-07-14_23-14-04.png)
![2024-04-30_18-00-01](https://codeberg.org/Aalexeey/gtk-theme-raleigholive/raw/branch/main/2024-04-30_18-00-01.png)
![2023-01-09_22-09-51](https://codeberg.org/Aalexeey/gtk-theme-raleigholive/raw/branch/main/2023-01-09_22-09-51.png)